// console.log("Hello World");

//Functions 
	//Parameter and arguments

	function printName() {
		let nickname = prompt ("Enter your nickname:");
		console.log("Hi," + nickname);
	};
	// printName();




	function printName(name) {
		console.log("My name is " + name);

	};

	printName("Juana");

	//(name) - parameter
	//(juana) - argument

	printName("John");
	printName("Jane");

	let sampleVariable = "Yui";
	printName(sampleVariable);


	function checkDivisibilityBy8(num) {
		let remainder = num % 8;
		console.log("The remainder of " + num + "devided by 8 is: " + remainder);
		let isDivisibleBy8 = remainder === 0;
		console.log("Is" + num + "devisible by 8?");
		console.log(isDivisibleBy8);
	};
	checkDivisibilityBy8(64);
	checkDivisibilityBy8(28);


	function checkDivisibilityBy4(num) {
		let remainder = num % 4;
		console.log("The remainder of " + num + "devided by 4 is: " + remainder);
		let isDivisibleBy4 = remainder === 0;
		console.log("Is" + num + "devisible by 4?");
		console.log(isDivisibleBy4);
	};
	checkDivisibilityBy4(56);
	checkDivisibilityBy4(95);

	//function parameter can also accept other functions as arguments to perform more

	function argumentFunction() {
		console.log("This function was passed as an argument before the message was printed")
	};

	function invokeFunction(argumentFunction){
		argumentFunction();
	};
	invokeFunction(argumentFunction);

	console.log(argumentFunction);



	//multiple parameter - parameter and argument should have equal quantity

	function createFullName(firstName, middleName, lastName){
		console.log(firstName + ' ' + middleName + ' ' + lastName);
	}
	createFullName("Juan", "Dela", "Cruz");

	let firstName = "John";
	let middleName = "Doe";
	let lastName = "Smith";

	createFullName(firstName, middleName, lastName);

	function printfullName(middleName, firstName, lastName){
		console.log(firstName + ' ' + middleName + ' ' + lastName)
	}
	printfullName("Juan", "Dela", "Cruz")


	function printFriends(friend1, friend2, friend3){
		console.log("My 3 friends are: " + friend1 + ', ' + friend2 + ', ' + friend3);
	}
	printFriends("Juana", "John", "Jane");

	//Return statement
	function returnFullName(firstName, middleName, lastName){
		return firstName +' ' + middleName + ' ' + lastName;
		console.log("This message will not be printed.")
	}
	let completeName = returnFullName("Jeffrey", "Smith", "Bezos ");
	console.log(completeName);

	let completeName2 = returnFullName("Nehemiah", "C.", "Elorico");
	console.log(completeName2);

	let combination = completeName + completeName2;
	console.log(combination);

	console.log(returnFullName(firstName, middleName, lastName));

	function returnAddress(city, country){
		let fullAdress = city + " ," + country;
		return fullAdress;
	}

	let myAdress =returnAddress("Cebu City", "Philippines");
	console.log(myAdress);

	function printPlayerInfo(username, level, job){
		// console.log("username: " + username);
		// console.log("level: " + level);
		// console.log("job: " + job)
		return("username: " + username + "level: " + level + "job: " + job)
	}
	let user1 = printPlayerInfo("Knight_white", 95, "Paladin")
	console.log(user1)


function multiplyNumber(num1, num2){
	return num1 * num2;
}

let product = multiplyNumber(5, 10);
console.log("the product of 5 and 10 is:");
console.log(product);
	